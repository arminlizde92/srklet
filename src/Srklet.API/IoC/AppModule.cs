﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Srklet.BLL.Abstractions;
using Srklet.BLL.EF;
using Srklet.DAL;

namespace Srklet.API
{
    /// <summary>
    /// App Module
    /// </summary>
    public class AppModule : Module
    {
        /// <summary> private readonly instance of <see cref="IConfiguration"/> </summary>
        private readonly IConfiguration _config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config">instance of <see cref="IConfiguration"/></param>
        public AppModule(IConfiguration config) => _config = config;

        /// <summary>
        /// Registering types
        /// </summary>
        /// <param name="containerBuilder">instance of <see cref="ContainerBuilder"/></param>
        protected override void Load(ContainerBuilder containerBuilder)
        {
            // gets connection string
            string connectionString = _config.GetConnectionString("SrkletDbContext");

            // configuring settings
            var optionsBuilder = new DbContextOptionsBuilder<SrkletDbContext>();
            optionsBuilder.UseSqlServer(connectionString, opt => opt.CommandTimeout(60));

            containerBuilder.RegisterType<SrkletDbContext>()
                .AsSelf()
                .WithParameter("options", optionsBuilder.Options)
                .InstancePerLifetimeScope()
                .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            containerBuilder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope()
                .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            containerBuilder.RegisterType<PostRepository>()
                .As<IPostRepository>()
                .InstancePerLifetimeScope()
                .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
        }
    }
}
