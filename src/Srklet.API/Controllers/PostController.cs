﻿using Microsoft.AspNetCore.Mvc;
using Srklet.BLL.Abstractions;
using Srklet.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Srklet.API.Controllers
{
    /// <summary>
    /// post controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        /// <summary> private instance of <see cref="IUnitOfWork"/></summary>
        private IUnitOfWork _uow;

        /// <summary>
        /// constructor with one parameter going to be filled via IoC
        /// </summary>
        /// <param name="uow">instance of <see cref="IUnitOfWork"/></param>
        public PostController(IUnitOfWork uow) => _uow = uow;

        /// <summary>
        /// creates post
        /// </summary>
        /// <param name="createDto">instance of <see cref="CreatePostDto"/></param>
        /// <returns>new post id</returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] CreatePostDto createDto)
        {
            int id = await _uow.Posts.CreateAsync(createDto);
            return Ok(id);
        }

        /// <summary> Gets all posts </summary>
        [HttpGet("All")]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<PostDto> posts = await _uow.Posts.GetAllAsync();
            return Ok(posts);
        }

        /// <summary>
        /// gets post by id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>instnace of <see cref="PostDto"/></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            PostDto postDto = await _uow.Posts.GetByIdAsync(id);
            return Ok(postDto);
        }
    }
}
