﻿using System.ComponentModel.DataAnnotations;

namespace Srklet.DAL
{
    /// <summary>
    /// entity for images
    /// </summary>
    public class Image
    {
        /// <summary> Identification number </summary>
        public int Id { get; set; }

        /// <summary> public id </summary>
        public string PublicId { get; set; }

        /// <summary> Tells where the media is found which site etc...</summary>
        public string Source { get; set; }

        /// <summary> Url to the media (absolute) </summary>
        [Required]
        public string Url { get; set; }
    }
}
