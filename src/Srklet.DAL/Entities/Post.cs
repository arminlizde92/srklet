﻿using System;

namespace Srklet.DAL
{
    /// <summary>
    /// Entity for posts
    /// </summary>
    public class Post
    {
        /// <summary> post id </summary>
        public int Id { get; set; }

        /// <summary> post creation date and time </summary>
        public DateTime Created { get; set; } = DateTime.Now;

        /// <summary> last date and time when post is modified </summary>
        public DateTime Modified { get; set; } = DateTime.Now;

        /// <summary> post title </summary>
        public string Title { get; set; }

        /// <summary> post text </summary>
        public string Text { get; set; }

        /// <summary> image id </summary>
        public int ImageId { get; set; }

        /// <summary> virtual instance of image </summary>
        public virtual Image Image { get; set; }

        /// <summary> post activity status </summary>
        public bool IsActive { get; set; } = true;
    }
}
