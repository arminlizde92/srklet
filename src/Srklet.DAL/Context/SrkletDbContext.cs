﻿using Microsoft.EntityFrameworkCore;

namespace Srklet.DAL
{
    /// <summary>
    /// Application db context
    /// </summary>
    public class SrkletDbContext : DbContext
    {
        /// <summary>
        /// Context constructor
        /// </summary>
        /// <param name="options">instnace of <see cref="DbContextOptions{VisitingStolacDbContext}"/></param>
        public SrkletDbContext(DbContextOptions<SrkletDbContext> options)
            : base(options)
        {
        }

        /// <summary> access to posts </summary>
        public DbSet<Post> Posts { get; set; }

        /// <summary> access to images </summary>
        public DbSet<Image> Images { get; set; }
    }
}
