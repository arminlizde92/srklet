﻿using Srklet.BLL.Abstractions;

namespace Srklet.BLL.EF
{
    /// <summary>
    /// unit of work implementation
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// post repository
        /// </summary>
        public IPostRepository Posts { get; set; }
    }
}
