﻿using CloudinaryDotNet;
using Microsoft.EntityFrameworkCore;
using Srklet.BLL.Abstractions;
using Srklet.Common;
using Srklet.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Srklet.BLL.EF
{
    /// <summary>
    /// implementation of the post repository
    /// </summary>
    public class PostRepository : IPostRepository
    {
        /// <summary> private instance of <see cref="SrkletDbContext"/> </summary>
        private SrkletDbContext _context;

        /// <summary>
        /// constructor with <see cref="SrkletDbContext"/> as param => DI will be executed
        /// </summary>
        /// <param name="context">instance of <see cref="SrkletDbContext"/></param>
        public PostRepository(SrkletDbContext context) => _context = context; 

        /// <summary>
        /// creates post
        /// </summary>
        /// <param name="dto">instance of <see cref="CreatePostDto"/></param>
        /// <returns>id of created post</returns>
        public async Task<int> CreateAsync(CreatePostDto dto)
        {
            CloudinaryImageOperationDto cloudinaryDto = await CloudinaryHelper.UploadAsync(dto.ImageUrl);

            if (cloudinaryDto.HttpStatusCode != HttpStatusCode.OK)
                throw new CloudinaryException($"Could not upload image from following url: {dto.ImageUrl}");

            Image img = new Image
            {
                PublicId = cloudinaryDto.PublicId,
                Url = cloudinaryDto.AbsoluteUrl,
                Source = dto.ImageSource
            };

            await _context.Images.AddAsync(img);
            await _context.SaveChangesAsync();

            Post post = new Post()
            {
                Text = dto.Text,
                Title = dto.Title,
                ImageId = img.Id
            };

            await _context.Posts.AddAsync(post);
            await _context.SaveChangesAsync();

            return post.Id;
        }

        /// <summary>
        /// gets all posts .. ordered by creation date descending
        /// </summary>
        /// <returns>instance of <see cref="IEnumerable{PostDto}"/></returns>
        public async Task<IEnumerable<PostDto>> GetAllAsync()
        {
            return await _context.Posts.Include(i => i.Image).Select(p => new PostDto
            {
                Id = p.Id,
                Created = p.Created,
                Modified = p.Modified,
                Text = p.Text,
                Title = p.Title,
                ImageUrl = p.Image.Url
            }).OrderByDescending(p => p.Created).ToListAsync();
        }

        /// <summary>
        /// gets object by id
        /// </summary>
        /// <param name="id">object id</param>
        /// <returns>instance of <see cref="PostDto"/></returns>
        public async Task<PostDto> GetByIdAsync(int id)
        {
            PostDto post = await _context.Posts
                .Include(i => i.Image)
                .Select(p => new PostDto {
                    Id = p.Id,
                    Created = p.Created,
                    Modified = p.Modified,
                    Text = p.Text,
                    Title = p.Title,
                    ImageUrl = p.Image.Url
                }).FirstOrDefaultAsync(p => p.Id == id);
                                      
            if (post == null)
                throw new KeyNotFoundException($"Post with id: {id} could not be found!");

            return post;
        }
    }
}
