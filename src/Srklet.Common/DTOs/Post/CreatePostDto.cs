﻿namespace Srklet.Common
{
    /// <summary>
    /// dto for creating post
    /// </summary>
    public class CreatePostDto
    {
        /// <summary> post title </summary>
        public string Title { get; set; }

        /// <summary> post text </summary>
        public string Text { get; set; }

        /// <summary> apsoulte image url </summary>
        public string ImageUrl { get; set; }

        /// <summary> image source etc.. google, book ... </summary>
        public string ImageSource { get; set; }
    }
}
