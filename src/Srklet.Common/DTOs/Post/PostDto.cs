﻿using System;

namespace Srklet.Common
{
    /// <summary>
    /// dto for presenting post
    /// </summary>
    public class PostDto
    {
        /// <summary> post id </summary>
        public int Id { get; set; }

        /// <summary> post creation date and time </summary>
        public DateTime Created { get; set; }

        /// <summary> last date and time when post is modified </summary>
        public DateTime Modified { get; set; }

        /// <summary> post title </summary>
        public string Title { get; set; }

        /// <summary> post text </summary>
        public string Text { get; set; }

        /// <summary> image url </summary>
        public string ImageUrl { get; set; }
    }
}
