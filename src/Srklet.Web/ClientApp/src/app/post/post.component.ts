import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html'
})
export class PostComponent {
  public posts: Post[];

  constructor(http: HttpClient) {
    http.get<Post[]>('https://localhost:44320/api/post/all').subscribe(result => { // changes
      this.posts = result;
    }, error => console.error(error));
  }
}

interface Post {
  created: Date;
  modified: Date;
  title: string;
  text: string;
}
