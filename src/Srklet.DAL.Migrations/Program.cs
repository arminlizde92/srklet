﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace Srklet.DAL.Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appSettings.development.json", optional: false, reloadOnChange: true);

            var factory = new SrkletDbContextFactory();

            using (SrkletDbContext context = factory.CreateDbContext(args))
            {
                Console.WriteLine("Starting migrations...");
                context.Database.Migrate();
            }

            Console.WriteLine("Completed ...");
            Console.ReadLine();
        }
    }
}
