﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Srklet.DAL.Migrations
{
    public class SrkletDbContextFactory : IDesignTimeDbContextFactory<SrkletDbContext>
    {
        /// <summary>
        /// creates db context
        /// </summary>
        /// <param name="args">console arguments</param>
        /// <returns>instance of <see cref="VisitingStolacDbContext"/></returns>
        public SrkletDbContext CreateDbContext(string[] args)
        {
            ConfigurationBuilder cfgBuilder = new ConfigurationBuilder();
            cfgBuilder.AddJsonFile("appSettings.json");
            cfgBuilder.AddJsonFile($"appSettings.development.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = cfgBuilder.Build();

            string connectionString = configuration.GetConnectionString("VisitingStolacDbContext");

            DbContextOptionsBuilder<SrkletDbContext> builder = new DbContextOptionsBuilder<SrkletDbContext>();

            builder.UseSqlServer(connectionString, optons => optons.EnableRetryOnFailure());
            builder.EnableSensitiveDataLogging();

            return new SrkletDbContext(builder.Options);
        }
    }
}
