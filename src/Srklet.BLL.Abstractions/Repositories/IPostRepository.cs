﻿using Srklet.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Srklet.BLL.Abstractions
{
    /// <summary>
    /// interface for post repository
    /// </summary>
    public interface IPostRepository
    {
        /// <summary>
        /// gets post by id
        /// </summary>
        /// <param name="id">post id</param>
        /// <returns>instance of <see cref="PostDto"/></returns>
        Task<PostDto> GetByIdAsync(int id);

        /// <summary>
        /// gets all posts
        /// </summary>
        /// <returns>instance of <see cref="IEnumerable{PostDto}"/></returns>
        Task<IEnumerable<PostDto>> GetAllAsync();

        /// <summary>
        /// creates post
        /// </summary>
        /// <param name="dto">instance of <see cref="CreatePostDto"/></param>
        /// <returns>id of the created post</returns>
        Task<int> CreateAsync(CreatePostDto dto);
    }
}
