﻿namespace Srklet.BLL.Abstractions
{
    /// <summary>
    /// Inteface for unit of work
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary> interface for post repository </summary>
        IPostRepository Posts { get; }

        /// <summary> method for commiting channges </summary>
        //Task CommitAsync();
    }
}
